export type Node = {
    key: string;
    label: string;
    title: string;
    children?: any;
};