import React from "react";
import type { Node } from "./types";
import { Tree } from './Tree';

interface ITreeList {
    data: Node[];
}

export const TreeList: React.FC<ITreeList> = ({ data }) => {

    if (!data.length) {
        return null;
    }

    return (
        <>
            <h2>Tree Visualization component</h2>
            <div className="d-tree">
                <Tree data={data} />
            </div>
        </>
    );
};
