import { TreeNode } from "./TreeNode";
import type { Node } from './types';

export const Tree = ({ data }: { data?: Node[] }) => {
    return (
        <div className="d-tree">
            <ul className="d-flex d-tree-container flex-column">
                {data?.map((tree) => {
                    return <TreeNode key={tree.key} node={tree} />
                })}
            </ul>
        </div>
    );
};
