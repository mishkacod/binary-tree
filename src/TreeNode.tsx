import { useState } from "react";
import { Tree } from "./Tree";
import type { Node } from './types';

export const TreeNode = ({ node }: { node: Node }) => {
    const [childVisible, setChildVisibility] = useState<boolean>(false);

    const hasChild = node.children ? true : false;

    const isTree = node.children?.some((item: any) => item.key);
    const numbers = node.children?.map((i: any) => <li key={i} className="d-tree-node">{i}</li>)

    return (
        <li className="d-tree-node">
            <div className="d-flex" onClick={() => setChildVisibility((v) => !v)}>
                <div className={`d-tree-head ${childVisible ? "active" : ""}`}>
                    {node.label}
                </div>
            </div>

            {hasChild && childVisible && (
                <div className="d-tree-content">
                    <ul className="d-flex d-tree-container flex-column">
                        {isTree ? <Tree data={node.children} /> : numbers}
                    </ul>
                </div>
            )}
        </li>
    );
};
